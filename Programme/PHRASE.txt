-------------------------------------------------------------------------------
      Object
Addr  code   Symbol   Mnemon  Operand     Comment
-------------------------------------------------------------------------------
             ; ******************************************************************
             ; Programme: PHRASE1.TXT        version PEP813 sous WINDOWS
             ;
             ;       version 1 - Programme qui lit puis affiche une liste de caractères
             ;                   et qui compte le nombre de chiffres (0-9).
             ;
             ;       auteur:         Bernard Martin
             ;       code permanent: non applicable
             ;       courriel:       martin.bernard@uqam.ca
             ;       date:           hiver 2019
             ;       cours:          INF2171
             ; ******************************************************************
             ;
0000  410055          STRO    bienvenu,d  ; message à afficher
0003  C80000          LDX     0,i         ; début de la chaine
             ;                              pointeur à la chaine de caractères lus
0006  C00000          LDA     0,i         ; efface le registre A
0009  4D013D boucle:  CHARI   chaine,x    ; lecture d'un caractère de chaine
000C  D5013D          LDBYTEA chaine,x    ; charge le caractère lu
000F  780001          ADDX    1,i         ; position suivante
0012  B0000A          CPA     "\n",i      ; dernier caractère ?
0015  0A0030          BREQ    finchain    ; oui, c'est terminé
0018  B00030          CPA     '0',i       ; entre 0 et 9 ?
001B  08002D          BRLT    paschiff    ; non, ce n'est pas un chiffre
001E  B00039          CPA     '9',i       
0021  10002D          BRGT    paschiff    ; non, ce n'est pas un chiffre
0024  C10127          LDA     compteur,d  ; compteur de chiffres + 1
0027  700001          ADDA    1,i         
002A  E10127          STA     compteur,d  
002D  040009 paschiff:BR      boucle      
             ;
0030  B80001 finchain:CPX     1,i         ; est-ce que le caractère ENTREE est le premier caractère lu
0033  0A0051          BREQ    fini        
0036  C00000          LDA     0,i         ; insérons le délimiteur de fin
0039  F5013D          STBYTEA chaine,x    ; de ligne
003C  410129          STRO    lue,d       ; message à afficher
003F  4100F1          STRO    combien,d   ; message à afficher
0042  390127          DECO    compteur,d  
0045  C80000          LDX     0,i         ; on recommence
0048  E90127          STX     compteur,d  ; ré-initialise le compteur de chiffres lus
004B  4100C9          STRO    entrez,d    
004E  040009          BR      boucle      
0051  41010B fini:    STRO    normal,d    ; message de terminaison
0054  00              STOP                
             ;
             ; variables
             ;                N.B. Il est préférable d'afficher un long message sur plusieurs lignes car la largeur
             ;                     de la fenêtre d'output de PEP/8 n'est que de 52 caractères par défaut.
0055  426965 bienvenu:.ASCII  "Bienvenue à ce programme d'affichage d'une phrase"
      6E7665 
      6E7565 
      20E020 
      636520 
      70726F 
      677261 
      6D6D65 
      206427 
      616666 
      696368 
      616765 
      206427 
      756E65 
      207068 
      726173 
      65     
0086  0A6C75          .ASCII  "\nlue."    
      652E   
008B  0A5665          .ASCII  "\nVeuillez entrer uniquement le caractère ENTREE"
      75696C 
      6C657A 
      20656E 
      747265 
      722075 
      6E6971 
      75656D 
      656E74 
      206C65 
      206361 
      726163 
      74E872 
      652045 
      4E5452 
      4545   
00BA  0A706F          .ASCII  "\npour terminer."
      757220 
      746572 
      6D696E 
      65722E 
00C9  0A0A45 entrez:  .ASCII  "\n\nEntrez une chaine de caractères svp: \x00"
      6E7472 
      657A20 
      756E65 
      206368 
      61696E 
      652064 
      652063 
      617261 
      6374E8 
      726573 
      207376 
      703A20 
      00     
00F1  0A4E6F combien: .ASCII  "\nNombre de chiffres lus: \x00"
      6D6272 
      652064 
      652063 
      686966 
      667265 
      73206C 
      75733A 
      2000   
010B  0A0A46 normal:  .ASCII  "\n\nFin normale du programme.\x00"
      696E20 
      6E6F72 
      6D616C 
      652064 
      752070 
      726F67 
      72616D 
      6D652E 
      00     
0127  0000   compteur:.BLOCK  2           ; #2d
0129  0A4C61 lue:     .ASCII  "\nLa chaine lue est: "
      206368 
      61696E 
      65206C 
      756520 
      657374 
      3A20   
             ; chaine DOIT être à la fin du programme
013D  000000 chaine:  .BLOCK  10          ; #1h10a pour voir les 10 premiers caractères
      000000 
      000000 
      00     
0147                  .BLOCK  0           ; le reste de la chaine
0147                  .END                  
-------------------------------------------------------------------------------


Symbol table
--------------------------------------
Symbol    Value        Symbol    Value
--------------------------------------
bienvenu  0055         boucle    0009
chaine    013D         combien   00F1
compteur  0127         entrez    00C9
finchain  0030         fini      0051
lue       0129         normal    010B
paschiff  002D         
--------------------------------------
------------------------------------------------------
Résultats de l'exécution
------------------------------------------------------
Bienvenue à ce programme d'affichage d'une phrase
lue.
Veuillez entrer uniquement le caractère ENTREE
pour terminer.

Entrez une chaine de caractères svp: INF2171

La chaine lue est: INF2171

Nombre de chiffres lus: 4

Entrez une chaine de caractères svp: 12

La chaine lue est: 12

Nombre de chiffres lus: 2

Entrez une chaine de caractères svp: 


Fin normale du programme.