-------------------------------------------------------------------------------
      Object
Addr  code   Symbol   Mnemon  Operand     Comment
-------------------------------------------------------------------------------
             ; ************************************************************
             ;       Programme: PREMIER3.TXT     version PEP813 sous Windows
             ;
             ;       version 3: version finale qui traite les débordements et qui demande la réexécution
             ;
             ;       Mon premier programme qui affiche la somme de 2 nombres signés.
             ;
             ;       auteur:         Bernard Martin
             ;       code permanent: non applicable
             ;       courriel:       martin.bernard@uqam.ca
             ;       date:           hiver 2019
             ;       cours:          INF2171
             ; ***********************************************************
             ;
0000  410040          STRO    bienvenu,d  ; message réel "Bienvenue" avec un e car les symboles sont limitées à 8 caractères
             ;
0003  4100FF début:   STRO    demande1,d  ; premier nombre demandé
0006  31003A          DECI    nombre1,d   ; lecture du premier nombre
0009  0A0036          BREQ    fini        ; le nombre 0 déclenche la fin du programme
000C  120030          BRV     débordem    ; il y a débordement si le nombre est > 32767 ou < -32768
000F  410138          STRO    demande2,d  ; deuxième nombre demandé
0012  31003C          DECI    nombre2,d   ; lecture du deuxième nombre
0015  120030          BRV     débordem    ; il y a débordement si le nombre est > 32767 ou < -32768
0018  C1003A          LDA     nombre1,d   ; premier nombre à traiter
001B  71003C          ADDA    nombre2,d   ; premier nombre + deuxième nombre
001E  120030          BRV     débordem    ; il y a débordement si la somme est > 32767 ou < -32768
0021  E1003E          STA     total,d     ; conservons le résultat
0024  41015E          STRO    réponse,d   ; affichage des résultats
0027  39003E          DECO    total,d     ; affiche le total en décimal
002A  50002E          CHARO   '.',i       ; fin de la phrase
002D  040003          BR      début       ; recommençons le processus
             ;
0030  41017E débordem:STRO    déborde,d   ; message de débordement, soit > 32767 (ou < -32768)
0033  040003 correct: BR      début       ; recommençons le processus
             ;
0036  4101BF fini:    STRO    termine,d   ; affichage de fin normale
0039  00              STOP                
             ;
             ; variables
             ;
003A  0000   nombre1: .BLOCK  2           ; #2d valeur initiale zéro
003C  0000   nombre2: .BLOCK  2           ; #2d valeur initiale zéro
003E  0000   total:   .BLOCK  2           ; #2d valeur initiale zéro
             ;
             ;                N.B. Il est préférable d'afficher un long message sur plusieurs lignes car la largeur
             ;                     de la fenêtre d'output de PEP/8 n'est que de 53 caractères par défaut.
0040  426965 bienvenu:.ASCII  "Bienvenue à ce programme d'additions de 2 nombres."
      6E7665 
      6E7565 
      20E020 
      636520 
      70726F 
      677261 
      6D6D65 
      206427 
      616464 
      697469 
      6F6E73 
      206465 
      203220 
      6E6F6D 
      627265 
      732E   
0072  0A566F          .ASCII  "\nVous devrez entrer 2 nombres compris"
      757320 
      646576 
      72657A 
      20656E 
      747265 
      722032 
      206E6F 
      6D6272 
      657320 
      636F6D 
      707269 
      73     
0097  0A656E          .ASCII  "\nentre -32768 et 32767."
      747265 
      202D33 
      323736 
      382065 
      742033 
      323736 
      372E   
00AE  0A0A50          .ASCII  "\n\nPour terminer le programme, veuillez entrer"
      6F7572 
      207465 
      726D69 
      6E6572 
      206C65 
      207072 
      6F6772 
      616D6D 
      652C20 
      766575 
      696C6C 
      657A20 
      656E74 
      726572 
00DB  0A6C65          .ASCII  "\nle chiffre 0 comme premier nombre.\x00"
      206368 
      696666 
      726520 
      302063 
      6F6D6D 
      652070 
      72656D 
      696572 
      206E6F 
      6D6272 
      652E00 
00FF  0A0A0A demande1:.ASCII  "\n\n\nVeuillez entrer le premier nombre"; précédé de 3 changements de ligne
      566575 
      696C6C 
      657A20 
      656E74 
      726572 
      206C65 
      207072 
      656D69 
      657220 
      6E6F6D 
      627265 
0123  0A2830          .ASCII  "\n(0-pour terminer): \x00"
      2D706F 
      757220 
      746572 
      6D696E 
      657229 
      3A2000 
0138  0A5665 demande2:.ASCII  "\nVeuillez entrer le deuxième nombre: \x00"
      75696C 
      6C657A 
      20656E 
      747265 
      72206C 
      652064 
      657578 
      69E86D 
      65206E 
      6F6D62 
      72653A 
      2000   
015E  0A4C65 réponse: .ASCII  "\nLe total de ces 2 nombres est \x00"
      20746F 
      74616C 
      206465 
      206365 
      732032 
      206E6F 
      6D6272 
      657320 
      657374 
      2000   
017E  0A4C65 déborde: .ASCII  "\nLe total excède le minimum(-32768) ou"
      20746F 
      74616C 
      206578 
      63E864 
      65206C 
      65206D 
      696E69 
      6D756D 
      282D33 
      323736 
      382920 
      6F75   
01A4  0A6C65          .ASCII  "\nle maximum permis(32767).\x00"
      206D61 
      78696D 
      756D20 
      706572 
      6D6973 
      283332 
      373637 
      292E00 
01BF  0A0A46 termine: .ASCII  "\n\nFin normale du programme.\x00"
      696E20 
      6E6F72 
      6D616C 
      652064 
      752070 
      726F67 
      72616D 
      6D652E 
      00     
01DB                  .END                  
-------------------------------------------------------------------------------


Symbol table
--------------------------------------
Symbol    Value        Symbol    Value
--------------------------------------
bienvenu  0040         correct   0033
demande1  00FF         demande2  0138
déborde   017E         débordem  0030
début     0003         fini      0036
nombre1   003A         nombre2   003C
réponse   015E         termine   01BF
total     003E         
--------------------------------------
-------------------------------------------------------------------------------
Exécution avec des nombres numériquement valides
-------------------------------------------------------------------------------
Bienvenue à ce programme d'additions de 2 nombres.
Vous devrez entrer 2 nombres compris
entre -32768 et 32767.

Pour terminer le programme, veuillez entrer
le chiffre 0 comme premier nombre.


Veuillez entrer le premier nombre
(0-pour terminer): 11

Veuillez entrer le deuxième nombre: 22

Le total de ces 2 nombres est 33.


Veuillez entrer le premier nombre
(0-pour terminer): 32000

Veuillez entrer le deuxième nombre: 1000

Le total excède le minimum(-32768) ou
le maximum permis(32767).


Veuillez entrer le premier nombre
(0-pour terminer): -32767

Veuillez entrer le deuxième nombre: -2

Le total excède le minimum(-32768) ou
le maximum permis(32767).


Veuillez entrer le premier nombre
(0-pour terminer): 32769

Le total excède le minimum(-32768) ou
le maximum permis(32767).


Veuillez entrer le premier nombre
(0-pour terminer): 0


Fin normale du programme.