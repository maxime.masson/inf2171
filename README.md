# Organisation des ordinateurs et Assembleur

## Description du cours

Familiariser l'étudiant avec le fonctionnement de l'ordinateur à partir des niveaux de l'assembleur, du langage machine et des circuits logiques.

Description des unités de l'ordinateur (processeur, mémoire, bus, périphériques). Représentation et manipulation de l'information (bits, octets, entiers signés et non signés, flottants, pointeurs, tableaux, enregistrements). Organisation et accès à la mémoire (pile, tas et leurs adressages). Représentation et exécution des programmes en langage machine et en assembleur (jeux d'instructions, sous-programmes, entrées-sorties).

## Objectifs du cours

- Acquérir une formation théorique et pratique sur les principes de fonctionnement des ordinateurs et des microprocesseurs
- Compréhension des concepts `fondamentaux` au niveau du matériel et du logiciel, à partir de la programmation en langage `Assembleur`.
- Maîtrise du processeur Pep8

## Sujets abordés

- Architecture générale d'un ordinateur (CPU, Mémoire, Disques, Périphérique)
- Codage de l'informatique arithmétique sur ordinateur (Addition, soustraction, multiplication, division)
- Codes de condition
- Concept des registres
- Intruction et directives en Assembleur
- Mode d'adressages et instructions du processeur Pep8
    - Branchement
    - Boucles 
    - Tableaux
    - Structure
    - Documentation
- Pile : Appels simples
- Sous-programmes
    - Appels
    - Paramètres
    - Utilisation de la pile
    - Sauvegarde des registres
    - Modes d'adressage sur la pile
- Instructions arithmétiques et logiques (Arithmétique réelle)
- Interruptions : Vecteurs d'interruption

## Énoncés des démonstrations (Démos)

Les laboratoires sont disponible sur le site web de [Bernard Martin](http://www.martinb.uqam.ca/)




